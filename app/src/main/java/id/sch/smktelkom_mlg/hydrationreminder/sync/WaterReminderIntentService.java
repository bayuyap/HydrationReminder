package id.sch.smktelkom_mlg.hydrationreminder.sync;

/**
 * Created by SMK TELKOM on 3/19/2018.
 */

import android.app.IntentService;
import android.content.Intent;

public class WaterReminderIntentService extends IntentService {
    public WaterReminderIntentService() {
        super("WaterReminderIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String action = intent.getAction();
        ReminderTasks.executeTask(this, action);
    }
}
